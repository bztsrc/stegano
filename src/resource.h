#ifndef RESOURCE_H
#define RESOURCE_H

#define IDC_STATIC              -1
#define IDI_APP_ICON            11

#define IDC_MAINDLG             101
#define IDC_MAINDLG_SOURCE      102
#define IDC_MAINDLG_SELECT      103
#define IDC_MAINDLG_PASS        104
#define IDC_MAINDLG_LOAD        105
#define IDC_MAINDLG_SAVE        106
#define IDC_MAINDLG_MSG         107
#define IDC_MAINDLG_STATUS      108

#endif
